package com.example.bibliothequenationale;


import java.util.ArrayList;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import Model.*;
import Adapter.*; 

public class NewsFragement extends Fragment {

	public static NewsFragement newInstance() {
		NewsFragement fragment = new NewsFragement();

        return fragment;
    }

    public NewsFragement() {
    	
    }

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ListView list = null;
        list = (ListView) rootView.findViewById(R.id.listnews);
        ArrayList<News> news = new ArrayList<News>();
        for (int i = 0; i < 20; i++) {
        	news.add(new News("anas","hmare"));
		}
        
        NewsAdapter adapter = new NewsAdapter(news, getActivity());
        
        list.setAdapter(adapter);
        
        return rootView;
   
	}
	
	
}
