package com.example.bibliothequenationale;

import java.util.ArrayList;

import Adapter.RessourcesAdapter;
import Model.Ressources;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class RessourcesFragment extends Fragment {
	
	public static RessourcesFragment newInstance() {
		RessourcesFragment fragment = new RessourcesFragment();

        return fragment;
    }

    public RessourcesFragment() {
    	
    }

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ressources, container, false);
        ListView list = null;
        list = (ListView) rootView.findViewById(R.id.listressources);
        ArrayList<Ressources> ressources = new ArrayList<Ressources>();
      
        	ressources.add(new Ressources("Notre catalogue en ligne"));
        	ressources.add(new Ressources("Catalogue du maroc"));
        	ressources.add(new Ressources("Le catalogue en ligne du Fonds de la « Source »"));
        	ressources.add(new Ressources("Bouquets électroniques"));
        	ressources.add(new Ressources("Nos collections"));
        	ressources.add(new Ressources("publication marocaines"));
		
		
        
        RessourcesAdapter adapter = new RessourcesAdapter(ressources, getActivity());
        
        list.setAdapter(adapter);
        
        return rootView;
   
	}
	
}
