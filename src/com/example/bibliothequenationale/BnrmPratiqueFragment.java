package com.example.bibliothequenationale;

import java.util.ArrayList;

import Adapter.BnrmPratiqueAdapter;
import Model.BnrmPratique;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class BnrmPratiqueFragment extends Fragment {
	public static BnrmPratiqueFragment newInstance() {
		BnrmPratiqueFragment fragment = new BnrmPratiqueFragment();

        return fragment;
    }

    public BnrmPratiqueFragment() {
    	
    }
    
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bnrmpratique, container, false);
         ListView list = null;
        list = (ListView) rootView.findViewById(R.id.listbnrmpratique);
        ArrayList<BnrmPratique> bnrmpratique = new ArrayList<BnrmPratique>();
      
        	bnrmpratique.add(new BnrmPratique("Horaires d�ouverture"));
        	bnrmpratique.add(new BnrmPratique("Plan d�acc�s"));
        	bnrmpratique.add(new BnrmPratique("Adh�sion"));
        	bnrmpratique.add(new BnrmPratique("Acc�s aux salles de lecture"));
        	bnrmpratique.add(new BnrmPratique("Formation usagers"));
        	bnrmpratique.add(new BnrmPratique("Services aux publics � besoins sp�cifiques"));
		
		
        
        BnrmPratiqueAdapter adapter = new BnrmPratiqueAdapter(bnrmpratique, getActivity());
        
        list.setAdapter(adapter);
        
        return rootView;
        
     	}
}
