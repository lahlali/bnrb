package Adapter;

import java.util.ArrayList;

import com.example.bibliothequenationale.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import Model.*;

public class NewsAdapter extends BaseAdapter {
	
	private ArrayList<News> news;
	private LayoutInflater inflater; 
	
	private class ViewHolder{
		TextView titre,contenu;
	}
	
	public NewsAdapter(ArrayList<News> news,Context context){
		this.news = news;
		this.inflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return news.size();
	}

	@Override
	public News getItem(int position) {
		// TODO Auto-generated method stub
		return news.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder=null;
		if (convertView == null){
			convertView = inflater.inflate(R.layout.item_news , null);
			holder = new ViewHolder();
			holder.titre= (TextView) convertView.findViewById(R.id.titlenews);
			holder.contenu = (TextView)convertView.findViewById(R.id.contenunews) ;
			
			holder.titre.setText(getItem(position).getTitre());
			holder.contenu.setText(getItem(position).getContenu());
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}

}
