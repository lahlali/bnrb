package Adapter;

import java.util.ArrayList;

import com.example.bibliothequenationale.R;

import Model.BnrmPratique;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BnrmPratiqueAdapter extends BaseAdapter{
	
	private ArrayList<BnrmPratique> bnrmpratique;
	private LayoutInflater inflater;
	
	public class ViewHolder{
		TextView titrebnrmpratique;
		ImageView image;
	}
	
	public BnrmPratiqueAdapter(ArrayList<BnrmPratique> bnrmpratique ,Context context){
		this.bnrmpratique = bnrmpratique;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return bnrmpratique.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return bnrmpratique.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder=null;

		if(convertView == null){
			convertView = inflater.inflate(R.layout.item_bnrmpratique , null);
			holder = new ViewHolder();
			holder.image = (ImageView) convertView.findViewById(R.id.bnrbimage);
			holder.image.setImageResource(getImage(position));
			holder.titrebnrmpratique= (TextView) convertView.findViewById(R.id.titlebnrmpratique);
			holder.titrebnrmpratique.setText(((BnrmPratique) getItem(position)).getTitrebnrm());
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}
	
	private int getImage(int position){
		switch (position) {
		case 0:
			return R.drawable.ic_horraire;
		case 1:
			return R.drawable.ic_planaccess;
		case 2:
			return R.drawable.ic_adhesion;
		case 3:
			return R.drawable.ic_sallelecture;
		case 4:
			return R.drawable.ic_formation;
		case 5:
			return R.drawable.ic_service;
		}
		return R.drawable.ic_launcher;
	}

}
