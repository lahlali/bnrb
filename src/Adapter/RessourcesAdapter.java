package Adapter;

import java.util.ArrayList;

import com.example.bibliothequenationale.ActivityTempo;
import com.example.bibliothequenationale.R;

import Model.Ressources;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RessourcesAdapter extends BaseAdapter implements android.view.View.OnClickListener {

	private ArrayList<Ressources> ressources;
	private LayoutInflater inflater; 
	private String[] images = {"anass","dfqs"};
	Context context;
	
	public class ViewHolder{
		TextView titreressources;
		ImageView image;
		LinearLayout ll;
	}
	
	public RessourcesAdapter(ArrayList<Ressources> ressources ,Context context){
		this.ressources = ressources;
		this.inflater = LayoutInflater.from(context);
		this.context = context;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return ressources.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return ressources.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder=null;
		if (convertView == null){
			convertView = inflater.inflate(R.layout.item_ressources , null);
			holder = new ViewHolder();
			holder.image = (ImageView) convertView.findViewById(R.id.icressource);
			holder.image.setImageResource(getImage(position));
			holder.titreressources= (TextView) convertView.findViewById(R.id.titleressources);
			holder.titreressources.setText(((Ressources) getItem(position)).getTitreressources());
			holder.ll = (LinearLayout) convertView.findViewById(R.id.ressource);
			holder.ll.setOnClickListener(this);
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder) convertView.getTag();
		}
			return convertView;
	}
	
	private int getImage(int position){
		switch (position) {
			case 0:
				return R.drawable.ic_onlinecat;
			case 1:
				return R.drawable.ic_morococat;
			case 2:
				return R.drawable.ic_catalog;
			case 3:
				return R.drawable.ic_bouquet;
			case 4:
				return R.drawable.ic_collection;
			case 5:
				return R.drawable.ic_publication;
		}
		return R.drawable.ic_launcher;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent i = new Intent(context,ActivityTempo.class);
		context.startActivity(i);
	}


}