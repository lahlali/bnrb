package Model;

public class Ressources {

	private String titreressources;
	
	public Ressources(String titreressources){
		
		this.titreressources=titreressources;
		
	}

	public String getTitreressources() {
		return titreressources;
	}

	public void setTitreressources(String titreressources) {
		this.titreressources = titreressources;
	}
	
}
