package Model;

public class BnrmPratique {
	
	private String titrebnrmpratique;
	
	public BnrmPratique(String titrebnrmpratique){
		this.titrebnrmpratique = titrebnrmpratique;
	}

	public String getTitrebnrm() {
		return titrebnrmpratique;
	}

	public void setTitrebnrm(String titrebnrmpratique) {
		this.titrebnrmpratique = titrebnrmpratique;
	}
}
